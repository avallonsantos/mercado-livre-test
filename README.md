# Mercado Livre - Teste front-end

Esse projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) v8.0.0.

# Observações iniciais

Dado o escopo do teste, acessei a documentação da API do Mercado Livre para construir o objeto final dos produtos e não obtive sucesso para gerar meu `access_token` para acessar as informações sensíveis dos usuários do ML. Ao tentar gerar recebi um erro 404 fazendo post.

Sob este cenário, analisando o layout final, construí o model do objeto baseado nas informações que foram exibidas, de forma a poupar alguns recursos da aplicação e otimizá-la. Meu foco foi em entregar uma aplicação funcional, fazendo-me valer dos conceitos da programação reativa.

Sob orientação da Carolina, optei por não me arriscar porquanto no mundo do React (framework que ainda não domino), e construí a aplicação em Angular 8.

Desenvolvi um site responsivo. O layout mobile foi feito baseado no desktop, com a diminuição proporcional dos elementos, bem como pensando na fluidez dos containers.

Os estilos (SCSS) estão dentro de `/src/assets`, bem como as imagens utilizadas para o desenvolvimento.

## Instalação

Após baixar a aplicação, por favor rode `npm install` para instalar todas as dependências do projeto. 

## Rodar o aplicação

Por favor, rode `ng serve` para incializar o teste em ambiente local. Em seu navegador de preferência, navegue até `http://localhost:4200/` para ver a aplicação funcionando.

## Build

Por favor, rode `ng build` para construir a build final do projeto, caso se faça necessário. Os assets finais serão exportados para `/dist`.