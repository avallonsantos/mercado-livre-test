import { Injectable } from '@angular/core';

// Importing http modules
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';

// Importing reactive modules & operators
import { Observable, from } from 'rxjs';
import { map, filter, catchError, mergeMap, concatMap } from 'rxjs/operators';
import { pipe } from 'rxjs';

// Importing product model
import { Product, Description } from './models/product.model';

// Router
import { ActivatedRoute, Router, Params } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  private APIURL = `https://api.mercadolibre.com/`;

  constructor(private httpClient: HttpClient, private _Router: Router) {
  }

  // Function to retrieve products

  getProducts(term: string): Observable<Product> {
     return this.httpClient.get<Product>(this.APIURL + `sites/MLA/search?q=${term}&limit=4`)
  }

  // Function to retrieve product by id

  getProductById(id: string): Observable<Product> {
    return this.httpClient.get<Product>(this.APIURL + `items/${id}`)
  }

  // Function to retrieve product´s description by id

  getProductDescription(id: string): Observable<Description> {
    return this.httpClient.get<Description>(this.APIURL + `items/${id}/description`);
  }
}
