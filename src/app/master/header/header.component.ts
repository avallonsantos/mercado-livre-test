import { Component, OnInit } from '@angular/core';

// Importing form modules

import { FormGroup, FormBuilder } from '@angular/forms';

// Router
import { ActivatedRoute, Router, Params } from '@angular/router';

// Initializing component
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  public query: string;
  public form: FormGroup;

  constructor(
    private _Router: Router,
    private _ActivatedRouter: ActivatedRoute,
    private _FormBuilder: FormBuilder) {
      this.form = this._FormBuilder.group({
        search_keywords: ['']
      })
  }

  public setQuery(event: Event): void {
    this.query = ((event.target as HTMLInputElement).value);
  }

  public doSearch(e): void {
    e.preventDefault();
    this._Router.navigate(['/items'], {queryParams: {
      search: this.query
    }});
  }

  ngOnInit() {
  
  }

}
