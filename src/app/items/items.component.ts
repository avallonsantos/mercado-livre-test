import { Component, OnInit } from '@angular/core';

// Importing API Class
import { ApiService } from '../api.service';

// Importing Product Model
import { Product } from '../models/product.model';

// Importing reactive JavaScript operators
import {catchError, tap, map, concatMap} from 'rxjs/operators';
import { pipe, Observable } from 'rxjs';

// Router
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  public searchQuery: string;
  public listProducts: Array<Product> = [];

  constructor(private _ApiService: ApiService, private _Router: Router, private _ActivatedRouter: ActivatedRoute) {
    this._Router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {

    this._ActivatedRouter.queryParams.subscribe((params => {
      if(params['search'] === null || params['search'] === undefined) {
        this._Router.navigate(['/']);
      }
      this.searchQuery = params['search'];
    }));

    this._ApiService.getProducts(this.searchQuery).pipe(map(res => res.results)).subscribe(result => {
      result.forEach(element => {
        const product: Product = new Product(
          element.id,
          element.title,
          element.price,
          element.thumbnail,
          element.address.city_name,
          element.condition
        );
        this.listProducts.push(product);
      });
    });
  }
}
