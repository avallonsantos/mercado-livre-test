import { Component, OnInit } from '@angular/core';

import { ApiService } from './../api.service';
import { Product, Description } from './../models/product.model';

// Importing reactive JavaScript operators
import {catchError, tap, map} from 'rxjs/operators';
import { pipe, Subscribable } from 'rxjs';

// Router
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  public product: Product;
  public showError: boolean = false;
  public productDescription: Description;
  public id: any;
  public paramsSub: any;

  constructor(private _ApiService: ApiService, private _Router: Router, private _ActivatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this._ActivatedRouter.params.subscribe(params => {
      if(params['id'] === null || params['id'] === undefined) {
        this._Router.navigate(['/']);
      }
      this.id = params['id']
    });

    this._ApiService.getProductById(this.id).subscribe(
      (product) => {
        this.product = product;
      },
      (error) => {
        this.showError = true;
        console.log(error);
      }
    )
   
    this._ApiService.getProductDescription(this.id).subscribe(description => this.productDescription = description);
  }

}
;