// Create product model

export class Product {
  results: any = [];
  id; title; price; thumbnail; condition; city_name;
 
  constructor(id: string, title: string, price: number, thumbnail: string, city: string, condition: string) {
    this.id = id;
    this.title = title;
    this.price = price;
    this.thumbnail = thumbnail;
    this.city_name = city;
    this.condition = condition;
  }
}

// Create Description model

export interface Description {
  results: any;
  text: string;
  plain_text: string;
}